<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02: ACTIVITY 1</title>
</head>
<body>
	<h1>Array Manipulation</h1>
	<h3>Divisibles of Five</h3>
	<?php 	for($counter = 5 ; $counter<=500;  $counter+=5) { 	?>
	<?php 		echo 	$counter; 								?>
	<?php 	} 													?>
	


	<h3>Students</h3>
	<ul>
		<li><p>Create an empty array named "students"</p></li>
		<?php $students = []; ?>
		<p>&nbsp Students = <?php print_r($students); 		?></p>

		<li><p>Accept a name of a Student and add it to the student array</p></li>
		<?php array_push($students, "John Smith"); ?>

		<li><p>Print the name added so far in the student array</p></li>
		<p>&nbspStudents = <?php print_r($students); 		?></p>

		<li><p>Count the number of names in the student array</p></li>
		<p>&nbsp Students Count = <?php echo count($students); 		?></p>

		<li><p>Add another student then print the array and its new count</p></li>
		<?php array_push($students, "Jane Smith"); ?>
		<p>&nbsp Students = <?php print_r($students); 		?></p>
		<p>&nbsp Students Count = <?php echo count($students); 		?></p>

		<li><p>Finally, remove the first student and print the array and its count</p></li>
		<?php array_shift($students); ?>
		<p>&nbsp Students = <?php print_r($students); 		?></p>
		<p>&nbsp Students Count = <?php echo count($students); 		?></p>		
	</ul>




</body>
</html>
