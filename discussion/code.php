<?php 
//[SECTION] Repetition Control Structure
	// It is used to execute code multiple times.

	// While Loop
		// A while loop takes a single condition.


function whileLoop(){
	$count = 5;
	while ($count !== 0){
		// echo $count;
		echo $count.'<br/>';
		$count --;
	}
}

function whileLoop1($count){
	while ($count !== 0){
		// echo $count;
		echo $count.'<br/>';
		$count --;
	}
}

	// Do-While Loop
		// A do-while loop works a lot like the while loop. the only difference is that, do, while guarantee that the code will execute at least once

function doWhileLoop(){
	$count = 20;
	do{
		echo $count.'<br>';
		$count--;
	}while ($count>0);
}


	//For Loop
		//for (initialValue; condition; iteration){
		 	//code block
		//}

function forLoop(){
	for ($count = 0; $count <= 20; $count++){
		echo $count.'<br>';
	}
}

	// Continue and Break Statement
		// "Continue" is a keyword that allows the code to go the next loop without finishing the current code block;
		// "Break" on the otherhand is a keyword that stop the execution of the current loop

function modifiedForLoop() {
	for ($count = 0; $count <= 20; $count++){
		if ($count % 2 === 0) 
			continue;
		echo $count.'<br>';
		if ($count > 10) 
			break;	
	}
}


// [SECTION] Array Manipulation

	// An Array is a kind of variable that can hold more than one value
	// Arrays are declared using array() function or square brackers []

		// In the earlier version of php, we cannot use [], but as of php 5.4 we can use the short array syntax whicj replace array() with []

//before Php 5.4
$studentNumbers1 = array ('2020-1923', '2020-1924', '2020-1965', '2020-1926', '2020-1927' );

//after Php 5.4
$studentNumbers2 = ['2020-1923', '2020-1924', '2020-1965', '2020-1926', '2020-1927' ];

//Simple Arrays
$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ['Acer', 'Asus','Lenovo','Neo','Redfox', 'Gateway','Toshiba', 'Fujitsu'];
$tasks= [
	'Write HTML',
	'eat php',
	'inhale css',
	'bake javescript'
];

//Associative Arrays
	// Associative Array differs from numeric array i the sense that associative arrays uses descriptive names in naming the elements/values (key=>value pair)
	// Double Arraw Operator (=>) - an assignment operator that is commnly used in the creation of associative array.

$gradePeriods = [
	'firstGrading'  => 98.5,
	'secondGrading' => 94.3,
	'thirdGrading'  => 89.2,
	'fourthGrading' => 90.1
];



// Two-Dimentional Array
	// 2-D Array is commonly used in image processing, grood example of this is our viewing screen that used multidementional array of pixels

$heroes = [
	['ironman',   'thor',     'hulk'],
	['wolverine', 'cyclops',  'jean gray'],
	['batman',    'superman', 'wonder woman']
];


// Two Dimensional Associative Array

$ironManPowers = [
	// 'regular' & 'signature' will be the label
	// 'power' is the item inside the label

	'regular'	=> ['repulser blast', 'rocket punch'],
	'signature' => ['unibeam']
];

// Array Iterrative method: foreach()

// foreach($heroes as $hero)
// 	print_r($hero);

// [Section] Array Mutators
// Array Mutators modift the content of an array

// Array Sorting

$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;


sort($sortedBrands);
rsort($reverseSortedBrands);

 ?>