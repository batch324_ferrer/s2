<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02: Selection Control Structure and Array Manipulation</title>
</head>
<body>
	<h1>Repetition Control Structure</h1>
	<h2>While Loop</h2>
	<?php whileLoop(); ?>
	<?php whileLoop1(10); ?>

	<h2>Do-While Loop</h2>
	<?php doWhileLoop(); ?>

	<h2>For Loop</h2>
	<?php forLoop() ?>

	<h2>Continue and Break</h2>
	<?php modifiedForLoop() ?>

	<h1>Array Manipulation</h1>
	<h2>Types of Array</h2>



	<h3>Simple Array</h3>
	<ul>
		<!-- php codes /statement can be breakdown using php tag -->
		<?php   foreach($computerBrands as $brand){                ?>
			  
			       <li> <?php echo $brand ?> </li>
			        <!-- php includes a shorthand for "php echo tag" -->
			       <li> <?= $brand; ?> </li>
		<?php   }                                                  ?>
	</ul>




	<h3>Associative  Array</h3>
	<ul>
		<!-- php codes /statement can be breakdown using php tag -->
		<?php   foreach($gradePeriods as $period => $grade){       ?>			 
		
			    	<li> 
			    		Grade in <?= $period; ?> is <?= $grade; ?>
			   		</li>
		<?php   }                                                  ?>
	</ul>




	<h3>Multidimensional  Array</h3>
	<ul>
		<?php 	
			foreach ($heroes as $team){
				foreach($team as $member){
		?> <!-- close the php tag before <li> tag -->						
					<li>	<?php echo 	$member ?></li>
		<!-- open a new php tag to eclose code block -->
		<?php
				}
			}
		?>
	</ul>


	<h3>Multi-Dimensional Associative Array</h3>
	<ul>	
		<?php 	
				foreach ($ironManPowers as $label => $powerGroup){
					foreach($powerGroup as $power){
		?>
						<li>	<?= "$label : $power" ?></li>						
		<?php
					}
				}
		?>		
	</ul>


	<h1>Mutators</h1>

	<h3>Sorting</h3>

	<p><?php print_r($sortedBrands); 						?></p>
	<p><?php print_r($reverseSortedBrands); 				?></p>

	<h3>Push</h3>
	<?php array_push($computerBrands, 'Apple'); 			?>
	<p><?php print_r($computerBrands); 						?></p>


	<h3>UNshift</h3>
	<?php array_unshift($computerBrands, 'Dell'); 			?>
	<p><?php print_r($computerBrands); 						?></p>

	<h3>POP</h3>
	<?php array_pop($computerBrands); 						?>
	<p><?php print_r($computerBrands); 						?></p>

	<h3>Shift</h3>
	<?php array_shift($computerBrands); 					?>
	<p><?php print_r($computerBrands); 						?></p>

	<h3>Count</h3>
	<p><?php echo count($computerBrands); 					?></p>

	<h3>In array</h3>
	<?php 	$acer = 'Acer'; ?>
	<p><?php var_dump(in_array($acer, $computerBrands));	?></p>




</body>
</html>